package com.dxc.service.implementation;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dxc.entity.Role;
import com.dxc.entity.User;
import com.dxc.payload.UserRegistrationDTO;
import com.dxc.repository.UserRepository;
import com.dxc.service.UserService;

@Service

public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	private UserRegistrationDTO mapToDTO(User user) {
		UserRegistrationDTO registrationDTO = new UserRegistrationDTO();
		registrationDTO.setFirstName(user.getFirstName());
		registrationDTO.setLastName(user.getLastName());
		registrationDTO.setEmail(user.getEmail());
		registrationDTO.setPassword(user.getPassword());

		return registrationDTO;
	}

	private User mapToEntity(UserRegistrationDTO registrationDTO) {
		User user = new User();
		user.setFirstName(registrationDTO.getFirstName());
		user.setLastName(registrationDTO.getLastName());
		user.setEmail(registrationDTO.getEmail());
		user.setPassword(passwordEncoder.encode(registrationDTO.getPassword()));
		user.setRoles(Arrays.asList(new Role("ROLE_USER")));

		return user;
	}

	@Override
	public User save(UserRegistrationDTO registrationDTO) {
		User user = mapToEntity(registrationDTO);
		
		return userRepository.save(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(username);
		if(user == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}
		
		return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(), mapRolesToAuthorities(user.getRoles()));
	}
	
	private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
		return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
	}
	
}
