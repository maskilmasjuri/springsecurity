package com.dxc.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.dxc.entity.User;
import com.dxc.payload.UserRegistrationDTO;

public interface UserService extends UserDetailsService {
	User save(UserRegistrationDTO registrationDTO);
}
